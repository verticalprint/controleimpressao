#include <mySD.h>
#define npixels 4 // numero de pixels da mesma cor da impressora
#define ncolunas 4 //Numero de colunas a serem lidas de cada vez
#define MOSI 23
#define MISO 19
#define SCK 18
#define CS 5
#define NOMEARQUIVO "T80.TXT"

File fcian1, fcian2, fcian3, fcian4, ponteiro;
File fmagenta1, fmagenta2, fmagenta3, fmagenta4;
File fyellow1, fyellow2, fyellow3, fyellow4;
File fblack1, fblack2, fblack3, fblack4;

char m1, m2, m3, m4;
char c1, c2, c3, c4; 
char ye1, ye2, ye3, ye4;
char b1, b2, b3, b4;

int pos1, pos2, pos3, pos4;
bool parar = false;
bool divisaoInteira = false;

char caracterLido = '0';
int pont_cnt = 0;
String largura_str = "";
String altura_str = "";
int largura, altura;
int multiplicador, tamanho;
int posicaoMax;
int repeticoes = 0;
int colunasRestantes = 0;

void abrirArquivos();
void posicionamentoInicialPonteiros();
void atualiza4Ponteiros();
void le4ArquivosEnviaSerial();
void leArquivosParciais(int nArquivos);

void setup()
{
  Serial.begin(115200);
  Serial2.begin(115200);
  
  Serial.print("\nIniciando SD: ");
  if (!SD.begin(CS, MOSI, MISO, SCK)){ Serial.println("FALHA!"); return;} 
  else Serial.println("OK"); //Testa se o SD for iniciado corretamente e imprime o teste
    
  abrirArquivos();
  posicionamentoInicialPonteiros();
    
  if (!fcian1){ Serial.println("Falha ao abrir o arquivo...");
  } else {
     //Lê o cabeçalho
    // Lê altura e largura
     caracterLido = fcian1.read(); //lê r
     caracterLido = fcian1.read();
     while (caracterLido != 'x'){
        largura_str.concat(caracterLido);
        caracterLido = fcian1.read();
     }

     //Serial.println("largura_str:" + largura_str);

     caracterLido = fcian1.read();
     while (caracterLido != 'c'){
        altura_str.concat(caracterLido);
        caracterLido = fcian1.read();
     }

//      Serial.println("altura_str:" + altura_str);

     largura = largura_str.toInt();
     altura = altura_str.toInt();

//      Serial.print("altura:");
//      Serial.println(altura);
//      Serial.print("largura:");
//      Serial.println(largura);

     //numero inteiro de repetições
     repeticoes = largura/(ncolunas*npixels);
     //repeticoes = altura/(ncolunas*npixels);

     multiplicador = altura;

//      Serial.print("repeticoes:");
//      Serial.println(repeticoes);
//      Serial.print("\n divisaoInteira:");
//      Serial.print(largura%(ncolunas*npixels));
      
    if (largura%(ncolunas*npixels) == 0){
      divisaoInteira = true;
      Serial.print("divisaoInteira\n");
      
    } else{ 
      divisaoInteira = false;
      Serial.print("divisaoNAOInteira\n");
    }

    // Posiciona o ponteiro do cian depois do cabeçalho
    fcian2.seek(fcian1.position()+(ncolunas*multiplicador)); // posiciona o ponteiro depois do cabeçalho
    fcian3.seek(fcian1.position()+(2*ncolunas*multiplicador));
    fcian4.seek(fcian1.position()+(3*ncolunas*multiplicador));

    posicionamentoInicialPonteiros();
 
    if (repeticoes > 0){
      //Repeticoes é o número de vezes que as npixels(4) colunas estão 
      //inteiras para serem impressas no arquivo 
      for (int i = 0; i < repeticoes; i++) {
        for (int j = 0; j < (multiplicador*ncolunas); j++) {
            Serial.print(i);
            Serial.print(j);
            le4ArquivosEnviaSerial();
         }
          // atualiza os ponteiros apenas para repeticoes-1
          if (i < repeticoes-1){ atualiza4Ponteiros();}
      }

      //A ultima repetição é realizada de forma separada
      colunasRestantes = largura%(ncolunas*npixels);

      Serial.print("\ncolunasRestantes = ");
      Serial.println(colunasRestantes);

      if (colunasRestantes != 0) { // Restam menos de 16 colunas
        if (colunasRestantes <= ncolunas){ 
          //Anda com um arquivo só
          //Serial.print("colunasRestantes < ncolunas\n");
  
          atualizaPonteirosParcial(1);
          for (int i = 0; i < (colunasRestantes*multiplicador) ; i++) { 
            leArquivosParciais(1); 
          }
        } else if (colunasRestantes <= 2*ncolunas){ 
        //Anda com 2 arquivos
          Serial.print("colunasRestantes < 2xncolunas\n");
  
          atualizaPonteirosParcial(2);
  
            //Divide por 2 porque são dois caracteres lidos ao mesmo tempo
            for (int i = 0; i < (colunasRestantes*multiplicador)/2 ; i++) { 
                leArquivosParciais(2);
            }
        } else if (colunasRestantes <= 3*ncolunas){ 
          Serial.print("colunasRestantes < 3xncolunas\n");
            //Anda com 3 arquivos
      
            atualizaPonteirosParcial(3);
            
            //Divide por 3 porque sao 3 caracteres lidos ao mesmo tempo
            for (int i = 0; i < (colunasRestantes*multiplicador)/3 ; i++) { 
                leArquivosParciais(3);     
            }
        }
      }
   }


  } // fim do else - arquivo abriu com sucesso
  Serial.println("Prontinho!");
}

void loop() {
  // put your main code here, to run repeatedly:
}

void abrirArquivos(){
  
  // abrir todos os ponteiros de todas as cores aqui
  fcian1 = SD.open(NOMEARQUIVO);
  fcian2 = SD.open(NOMEARQUIVO);
  fcian3 = SD.open(NOMEARQUIVO);
  fcian4 = SD.open(NOMEARQUIVO);

  //abre todos os arquivos magentas
  fmagenta1 = SD.open(NOMEARQUIVO);
  fmagenta2 = SD.open(NOMEARQUIVO);
  fmagenta3 = SD.open(NOMEARQUIVO);
  fmagenta4 = SD.open(NOMEARQUIVO);

  //abre todos os arquivos amarelos
  fyellow1 = SD.open(NOMEARQUIVO);
  fyellow2 = SD.open(NOMEARQUIVO);
  fyellow3 = SD.open(NOMEARQUIVO);
  fyellow4 = SD.open(NOMEARQUIVO);

  //abre todos os arquivos amarelos
  fblack1 = SD.open(NOMEARQUIVO);
  fblack2 = SD.open(NOMEARQUIVO);
  fblack3 = SD.open(NOMEARQUIVO);
  fblack4 = SD.open(NOMEARQUIVO);
}

//Esse procedimento realiza o posicionamento 
//inicial dos ponteiros dos arquivos
void posicionamentoInicialPonteiros(){
  
  ponteiro = SD.open(NOMEARQUIVO);
  pont_cnt = 0;

// Posicionamento inicial dos ponteiros
  while (caracterLido != 'k')
  {
     caracterLido = ponteiro.read();
     pont_cnt = pont_cnt+1;

     // Posiciona o ponteiro do magenta no local do primeiro caracter depois do m
     if (caracterLido == 'm'){
        //Serial.print("m=");
        //  Serial.println(pont_cnt);
        fmagenta1.seek(pont_cnt);
        fmagenta2.seek(pont_cnt+(ncolunas*multiplicador));
        fmagenta3.seek(pont_cnt+(2*ncolunas*multiplicador));
        fmagenta4.seek(pont_cnt+(3*ncolunas*multiplicador));

        // Posiciona o ponteiro do amarelo no local do primeiro caracter depois do y
     } else if (caracterLido == 'y'){
        //  Serial.print("y=");
        //  Serial.println(pont_cnt);
        fyellow1.seek(pont_cnt);
        fyellow2.seek(pont_cnt+(ncolunas*multiplicador));
        fyellow3.seek(pont_cnt+(2*ncolunas*multiplicador));
        fyellow4.seek(pont_cnt+(3*ncolunas*multiplicador));

     // Posiciona o ponteiro do preto no local do primeiro caracter depois do k
     } else if (caracterLido == 'k'){
        //Serial.print("k=");
        //Serial.println(pont_cnt);
        fblack1.seek(pont_cnt);
        fblack2.seek(pont_cnt+(ncolunas*multiplicador));
        fblack3.seek(pont_cnt+(2*ncolunas*multiplicador));
        fblack4.seek(pont_cnt+(3*ncolunas*multiplicador));

     }
  }
}

void le4ArquivosEnviaSerial(){
    
    //Lê os caracteres do Cian
    c1 = fcian1.read();
    c2 = fcian2.read();
    c3 = fcian3.read();
    c4 = fcian4.read();

    //Lê os caracteres do Magenta
    m1 = fmagenta1.read();
    m2 = fmagenta2.read();
    m3 = fmagenta3.read();
    m4 = fmagenta4.read(); 

    //Lê os caracteres do Amarelo
    ye1 = fyellow1.read();
    ye2 = fyellow2.read();
    ye3 = fyellow3.read();
    ye4 = fyellow4.read(); 

    //Lê os caracteres do Preto
    b1 = fblack1.read();
    b2 = fblack2.read();
    b3 = fblack3.read();
    b4 = fblack4.read(); 
              
    //Escreve na serial
    //Envia os bits correspondentes das cores no formato:
    // @ccccmmmmyyyykkkk
    // Precisou separar todos os caracteres para a serial "entender"
    Serial.print('@');
    Serial.print(c1);
    Serial.print(c2);
    Serial.print(c3);
    Serial.print(c4);
    Serial.print(m1);
    Serial.print(m2);
    Serial.print(m3);
    Serial.print(m4);
    Serial.print(ye1);
    Serial.print(ye2);
    Serial.print(ye3);
    Serial.print(ye4);
    Serial.print(b1);
    Serial.print(b2);
    Serial.print(b3);
    Serial.println(b4);

   //Escreve na impressora
   //Envia os bits correspondentes das cores no formato:
   // @ccccmmmmyyyykkkk
   // Precisou separar todos os caracteres para a serial "entender"
   Serial2.print('@');
   Serial2.print(c1);
   Serial2.print(c2);
   Serial2.print(c3);
   Serial2.print(c4);
   Serial2.print(m1);
   Serial2.print(m2);
   Serial2.print(m3);
   Serial2.print(m4);
   Serial2.print(ye1);
   Serial2.print(ye2);
   Serial2.print(ye3);
   Serial2.print(ye4);
   Serial2.print(b1);
   Serial2.print(b2);
   Serial2.print(b3);
   Serial2.println(b4);
}

void atualiza4Ponteiros(){
            
    // Alinhamento dos ponteiros do cian
    fcian1.seek(((ncolunas-1)*npixels*multiplicador)+fcian1.position());
    fcian2.seek(((ncolunas-1)*npixels*multiplicador)+fcian2.position());
    fcian3.seek(((ncolunas-1)*npixels*multiplicador)+fcian3.position());
    fcian4.seek(((ncolunas-1)*npixels*multiplicador)+fcian4.position());

    // Alinhamento dos ponteiros do magenta
    fmagenta1.seek((ncolunas-1)*npixels*multiplicador+fmagenta1.position());
    fmagenta2.seek((ncolunas-1)*npixels*multiplicador+fmagenta2.position());
    fmagenta3.seek((ncolunas-1)*npixels*multiplicador+fmagenta3.position());
    fmagenta4.seek((ncolunas-1)*npixels*multiplicador+fmagenta4.position());

    // Alinhamento dos ponteiros do amarelo
    fyellow1.seek((ncolunas-1)*npixels*multiplicador+fyellow1.position());
    fyellow2.seek((ncolunas-1)*npixels*multiplicador+fyellow2.position());
    fyellow3.seek((ncolunas-1)*npixels*multiplicador+fyellow3.position());
    fyellow4.seek((ncolunas-1)*npixels*multiplicador+fyellow4.position());

    // Alinhamento dos ponteiros do preto
    fblack1.seek((ncolunas-1)*npixels*multiplicador+fblack1.position());
    fblack2.seek((ncolunas-1)*npixels*multiplicador+fblack2.position());
    fblack3.seek((ncolunas-1)*npixels*multiplicador+fblack3.position());
    fblack4.seek((ncolunas-1)*npixels*multiplicador+fblack4.position());
}

void atualizaPonteirosParcial(int nArquivos){
  if (nArquivos == 1){
    //atualiza somente os conjuntos dos primeiros ponteiros
    fcian1.seek((ncolunas-1)*npixels*multiplicador+fcian1.position());
    fmagenta1.seek((ncolunas-1)*npixels*multiplicador+fmagenta1.position());
    fyellow1.seek((ncolunas-1)*npixels*multiplicador+fyellow1.position());
    fblack1.seek((ncolunas-1)*npixels*multiplicador+fblack1.position());
            
  } else if (nArquivos == 2){

        // Alinhamento dos ponteiros
    fcian1.seek((ncolunas-1)*npixels*multiplicador+fcian1.position());
    fcian2.seek((ncolunas-1)*npixels*multiplicador+fcian2.position());    
    fmagenta1.seek((ncolunas-1)*npixels*multiplicador+fmagenta1.position());
    fmagenta2.seek((ncolunas-1)*npixels*multiplicador+fmagenta2.position());
    fyellow1.seek((ncolunas-1)*npixels*multiplicador+fyellow1.position());
    fyellow2.seek((ncolunas-1)*npixels*multiplicador+fyellow2.position());
    fblack1.seek((ncolunas-1)*npixels*multiplicador+fblack1.position());
    fblack2.seek((ncolunas-1)*npixels*multiplicador+fblack2.position());

  } else if (nArquivos == 3){
    // Alinhamento dos ponteiros do cian
    fcian1.seek((ncolunas-1)*npixels*multiplicador+fcian1.position());
    fcian2.seek((ncolunas-1)*npixels*multiplicador+fcian2.position());
    fcian3.seek((ncolunas-1)*npixels*multiplicador+fcian3.position());
    
    // Alinhamento dos ponteiros do magenta
    fmagenta1.seek((ncolunas-1)*npixels*multiplicador+fmagenta1.position());
    fmagenta2.seek((ncolunas-1)*npixels*multiplicador+fmagenta2.position());
    fmagenta3.seek((ncolunas-1)*npixels*multiplicador+fmagenta3.position());
    
    // Alinhamento dos ponteiros do amarelo
    fyellow1.seek((ncolunas-1)*npixels*multiplicador+fyellow1.position());
    fyellow2.seek((ncolunas-1)*npixels*multiplicador+fyellow2.position());
    fyellow3.seek((ncolunas-1)*npixels*multiplicador+fyellow3.position());
    
    // Alinhamento dos ponteiros do preto
    fblack1.seek((ncolunas-1)*npixels*multiplicador+fblack1.position());
    fblack2.seek((ncolunas-1)*npixels*multiplicador+fblack2.position());
    fblack3.seek((ncolunas-1)*npixels*multiplicador+fblack3.position());
  }
}

void leArquivosParciais(int nArquivos){
  
  if (nArquivos == 1){
                 
      //Lê os caracteres das cores
      c1 = fcian1.read();
      m1 = fmagenta1.read();
      ye1 = fyellow1.read();
      b1 = fblack1.read();
  
      //Envia os bits correspondentes das cores no formato:
      // @ccccmmmmyyyykkkk
      //Serial2.print("@"+c1+'0'+'0'+'0'+m1+'0'+'0'+'0'+ye1+'0'+'0'+'0'+b1+'0'+'0'+'0');  
  //              //Serial.print('@'+c1+c2+c3+c4+m1+m2+m3+m4+ye1+ye2+ye3+ye4+b1+b2+b3+b4); 
     // Serial.print(i);
      Serial.print('@');
      Serial.print(c1);
      Serial.print('0');
      Serial.print('0');
      Serial.print('0');
      Serial.print(m1);
      Serial.print('0');
      Serial.print('0');
      Serial.print('0');
      Serial.print(ye1);
      Serial.print('0');
      Serial.print('0');
      Serial.print('0');
      Serial.print(b1);
      Serial.print('0');
      Serial.print('0');
      Serial.println('0');     
  
        //Escreve na impressora
      //Envia os bits correspondentes das cores no formato:
      // @ccccmmmmyyyykkkk
      // Precisou separar todos os caracteres para a serial "entender"
      Serial2.print('@');
      Serial2.print(c1);
      Serial2.print('0');
      Serial2.print('0');
      Serial2.print('0');
      Serial2.print(m1);
      Serial2.print('0');
      Serial2.print('0');
      Serial2.print('0');
      Serial2.print(ye1);
      Serial2.print('0');
      Serial2.print('0');
      Serial2.print('0');
      Serial2.print(b1);
      Serial2.print('0');
      Serial2.print('0');
      Serial2.println('0');
      
  } else if (nArquivos == 2){
    
    //Lê os caracteres das cores
    c1 = fcian1.read();
    c2 = fcian1.read();
    
    m1 = fmagenta1.read();
    m2 = fmagenta1.read();
    
    ye1 = fyellow1.read();
    ye2 = fyellow1.read();
    
    b1 = fblack1.read();
    b2 = fblack1.read();
   
    //Envia os bits correspondentes das cores no formato:
    // @ccccmmmmyyyykkkk
    //Serial2.print("@"+c1+c2+'0'+'0'+m1+m2+'0'+'0'+ye1+ye2+'0'+'0'+b1+b2+'0'+'0');
    //Serial.print("@"+c1+c2+'0'+'0'+m1+m2+'0'+'0'+ye1+ye2+'0'+'0'+b1+b2+'0'+'0');

    //Serial.print(i);
    Serial.print('@');
    Serial.print(c1);
    Serial.print(c2);
    Serial.print('0');
    Serial.print('0');
    Serial.print(m1);
    Serial.print(m2);
    Serial.print('0');
    Serial.print('0');
    Serial.print(ye1);
    Serial.print(ye2);
    Serial.print('0');
    Serial.print('0');
    Serial.print(b1);
    Serial.print(b2);
    Serial.print('0');
    Serial.println('0');

    //Escreve na impressora
    //Envia os bits correspondentes das cores no formato:
    // @ccccmmmmyyyykkkk
    // Precisou separar todos os caracteres para a serial "entender"
    Serial2.print('@');
    Serial2.print(c1);
    Serial2.print(c2);
    Serial2.print('0');
    Serial2.print('0');
    Serial2.print(m1);
    Serial2.print(m2);
    Serial2.print('0');
    Serial2.print('0');
    Serial2.print(ye1);
    Serial2.print(ye2);
    Serial2.print('0');
    Serial2.print('0');
    Serial2.print(b1);
    Serial2.print(b2);
    Serial2.print('0');
    Serial2.println('0');  

  } else if (nArquivos == 3){
    c1 = fcian1.read();
    c2 = fcian1.read();
    c3 = fcian1.read();
    
    m1 = fmagenta1.read();
    m2 = fmagenta1.read();
    m3 = fmagenta1.read();
    
    ye1 = fyellow1.read();
    ye2 = fyellow1.read();
    ye3 = fyellow1.read();
    
    b1 = fblack1.read();
    b2 = fblack1.read();
    b3 = fblack1.read();
                
    // @ccccmmmmyyyykkkk
    //Serial2.print("@"+c1+c2+c3+'0'+m1+m2+m3+'0'+ye1+ye2+ye3+'0'+b1+b2+b3+'0');
    //Serial.print('@'+c1+c2+c3+'0'+m1+m2+m3+'0'+ye1+ye2+ye3+'0'+b1+b2+b3+'0');
    //Serial.print(i);
    Serial.print('@');
    Serial.print(c1);
    Serial.print(c2);
    Serial.print(c3);
    Serial.print('0');
    Serial.print(m1);
    Serial.print(m2);
    Serial.print(m3);
    Serial.print('0');
    Serial.print(ye1);
    Serial.print(ye2);
    Serial.print(ye3);
    Serial.print('0');
    Serial.print(b1);
    Serial.print(b2);
    Serial.print(b3);
    Serial.println('0');  

    Serial2.print('@');
    Serial2.print(c1);
    Serial2.print(c2);
    Serial2.print(c3);
    Serial2.print('0');
    Serial2.print(m1);
    Serial2.print(m2);
    Serial2.print(m3);
    Serial2.print('0');
    Serial2.print(ye1);
    Serial2.print(ye2);
    Serial2.print(ye3);
    Serial2.print('0');
    Serial2.print(b1);
    Serial2.print(b2);
    Serial2.print(b3);
    Serial2.println('0'); 
  }
}
