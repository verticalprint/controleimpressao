# -*- coding: utf-8 -*-
import cv2
import sys
import os
import re
import subprocess
import numpy as np
from numpy import zeros

from PIL import Image
import scipy.misc as sc

import matplotlib

from matplotlib import image
from matplotlib import pyplot

def preencheMatriz(largura, altura):

    largura = int(largura)
    altura = int(altura)

    npsaida = np.zeros((altura, largura), dtype=np.int)

    i = 0
    j = 0

    for i in range(altura-1):
        for j in range(largura-1):
            npsaida[i][j] = int(i%10);

    return (npsaida)


def geraArquivoUnico(nome, largura, altura, matrizCian, matrizMagenta, matrizAmarela, matrizPreta):
    arq = open(nome,'w')
    arq.write('r'); #inicializador
    arq.write(str(largura));
    arq.write('x'); #separador
    arq.write(str(altura));
    
    arq.write('c'); #inicio do Cian
    for elem in matrizCian:
        for e in elem:
            arq.write(str(e));
            
    arq.write('m'); #inicio do Magenta
    for elem in matrizMagenta:
        for e in elem:
            arq.write(str(e));
            
    arq.write('y'); #inicio do Magenta
    for elem in matrizAmarela:
        for e in elem:
            arq.write(str(e));

    arq.write('k'); #inicio do Magenta
    for elem in matrizPreta:
        for e in elem:
            arq.write(str(e));
            

    arq.close();
    return arq
    
altura = int(input("Digite a altura:"));
largura = int(input("Digite a largura:"));

print(largura);
print(altura);

transpPreto = preencheMatriz(altura, largura);
transpCian = preencheMatriz(altura, largura);
transpAmarelo = preencheMatriz(altura, largura);
transpMagenta = preencheMatriz(altura, largura);


print(transpCian);
print(transpMagenta);
print(transpAmarelo);
print(transpPreto);


geraArquivoUnico("teste72.txt", largura, altura, transpCian, transpMagenta, transpAmarelo, transpPreto);



print("Prontinho!")
